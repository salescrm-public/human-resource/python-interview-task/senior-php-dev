## Instruction
1. Please copy the following questions in a Google doc and provide the answer to them.
2. Once you are done with them please share the link to the GDoc with `comment` access. Share the link of your GDoc with recruiting manager.
3. Please create a public repository in Gitlab and push your code for the task into that repository. Make sure that you create a ReadME.md and copy the task description there.
4. Once you are done with your code and merge your code into the public repository, share the link of your repository with recruiting manager. 
## Questions:

1.  What DB size they worked (DB name/version, #rows in the main table, what type of usual requests, what server size they used),  
    What issues were faced (most hard/interesting), how solved it?

    Did you work with a few millions record? describe how to handle it?

    Did you work with pre calculations? describe it.

    Did you work with views materializations? describe it.

    Did you work with replication DB? describe it.
    
2.  Does they work with queues, which, what issues were faced, how solved it
3.  What PHP framework they have used, what issues faced (most interesting) how solved
4.  Does your work with race condition? How to solve it?

## Task 
### Description of task
Please create the structure of classes/interfaces that you will create for this module (realization is not needed):
Let's imagine, that you should create a plugin, working with files in the project
Interface:
- put the file (metadata, file content)
- get file metadata
- get file content
- remove file
For the task  provide the following:
    1. Models folder and files
    2. Service file: how to use models file
    3. good to have the driver of how these files get inserted in the Database / S3 or any other storage service


<mark>Please not that we understand the question is not cristal clear and this is also part of the tests to see what assumption you will have in order to over come this challenge. Clearly in our projects all these assumptions will be provided as a part of tickets by BAs and PMs.</mark>

